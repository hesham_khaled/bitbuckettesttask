//
//  Bitbucket_TestTests.swift
//  Bitbucket TestTests
//
//  Created by Hesham Donia on 18/09/2021.
//

import XCTest
@testable import Bitbucket_Test

class Bitbucket_TestTests: XCTestCase {
    
    var userViewModel: UserViewModel!

    override func setUpWithError() throws {
       userViewModel = UserViewModel()
    }

    override func tearDownWithError() throws {
    
    }

    func testExample() throws {
        userViewModel.login(username: "enghesham33", password: "7asebatwma3lomat")
        userViewModel.loginNavigation.bind { loginNavigationResult in
            assert(loginNavigationResult == .DEFAULT)
        }
        
    }

}
