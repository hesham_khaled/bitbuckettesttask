//
//  UIView+Extensions.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

extension UIView {
    
    @IBInspectable public var shadow: Bool {
         get {
             return false
         }
         set {
             if newValue {
                self.addShadow(offset: CGSize(width: 0, height: 1), radius: 3, color: UIColor.lightGray, opacity: 0.3)
             }
         }
     }
    
    public func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    public func addShadow(offset: CGSize, radius: CGFloat, color: UIColor, opacity: Float, cornerRadius: CGFloat? = nil) {
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.shadowColor = color.cgColor
        if let r = cornerRadius {
            self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: r).cgPath
        }
        self.layer.masksToBounds = false
    }
    
    func applyNavBarConstraints(size: (width: Float, height: Float)) {
        let widthConstraint = self.widthAnchor.constraint(equalToConstant: CGFloat(size.width))
        let heightConstraint = self.heightAnchor.constraint(equalToConstant: CGFloat(size.height))
        heightConstraint.isActive = true
        widthConstraint.isActive = true
    }
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func setShadow(with color:UIColor) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
    }
    
    public func addTapGesture(tapNumber: Int = 1, target: AnyObject, action: Selector) {
            let tap = UITapGestureRecognizer(target: target, action: action)
            tap.numberOfTapsRequired = tapNumber
            addGestureRecognizer(tap)
            isUserInteractionEnabled = true
        }

    /// EZSwiftExtensions - Make sure you use  "[weak self] (gesture) in" if you are using the keyword self inside the closure or there might be a memory leak
    public func addTapGesture(tapNumber: Int = 1, action: ((UITapGestureRecognizer) -> Void)?) {
        let tap = BlockTap(tapCount: tapNumber, fingerCount: 1, action: action)
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            } else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            
        }
        get {
            return layer.cornerRadius
        }
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}

extension Dictionary {
    public func toString() -> String {
        
        var result = "{"
        
        for (key, value) in self {
            if value is String {
                result = result + "\"\(key)\"" + ":" + "\"\(value)\"" + ","
            } else if value is [Dictionary] {
                var jsonArrayString = "["
                if (value as! [Dictionary]).count > 0 {
                    for dict in (value as! [Dictionary]) {
                        jsonArrayString = jsonArrayString + dict.toString() + ","
                    }
                    jsonArrayString.removeLast()
                }
                jsonArrayString = jsonArrayString + "]"
                result = result + "\"\(key)\"" + ":" + "\(jsonArrayString)" + ","
            } else if value is Dictionary {
                let jsonObjectString = (value as! Dictionary).toString()
                result = result + "\"\(key)\"" + ":" + jsonObjectString + ","
            } else {
                result = result + "\"\(key)\"" + ":" + "\(value)" + ","
            }
            
        }
        
        result.removeLast()
        
        result = result + "}"
        result = result.replacingOccurrences(of: "(", with: "[")
        result = result.replacingOccurrences(of: ")", with: "]")
        result = result.replacingOccurrences(of: "<null>", with: "\"\"")
        //        print(result)
        return result
    }
    
    public func has(_ key: Key) -> Bool {
        return index(forKey: key) != nil
    }
}

extension Encodable {

    /// Converting object to postable dictionary
    func toDictionary(_ encoder: JSONEncoder = JSONEncoder()) throws -> [String: Any] {
        let data = try encoder.encode(self)
        let object = try JSONSerialization.jsonObject(with: data)
        guard let json = object as? [String: Any] else {
            let context = DecodingError.Context(codingPath: [], debugDescription: "Deserialized object is not a dictionary")
            throw DecodingError.typeMismatch(type(of: object), context)
        }
        return json
    }
}

extension String {
    
    func convertStringToDictionary() -> [String: Any]? {
                
        var newString = replacingOccurrences(of: "\n", with: "\\n")
        newString = newString.replacingOccurrences(of: "\t", with: "\\t")
        newString = newString.replacingOccurrences(of: "\r", with: "\\r")
        newString = newString.replacingOccurrences(of: "\\", with: "\\\\")
        if let data = newString.data(using: String.Encoding.utf8) {
            
            do {
                let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                
                return dictonary!
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func toBase64() -> String {
            return Data(self.utf8).base64EncodedString()
    }
}

open class BlockTap: UITapGestureRecognizer {
    private var tapAction: ((UITapGestureRecognizer) -> Void)?

    public override init(target: Any?, action: Selector?) {
        super.init(target: target, action: action)
    }

    public convenience init (
        tapCount: Int = 1,
        fingerCount: Int = 1,
        action: ((UITapGestureRecognizer) -> Void)?) {
            self.init()
            self.numberOfTapsRequired = tapCount

            #if os(iOS)

            self.numberOfTouchesRequired = fingerCount

            #endif

            self.tapAction = action
            self.addTarget(self, action: #selector(BlockTap.didTap(_:)))
    }

    @objc open func didTap (_ tap: UITapGestureRecognizer) {
        tapAction? (tap)
    }
}

