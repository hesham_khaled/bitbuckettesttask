//
//  UserDefaultsManager.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

class UserDefaultsManager {
    
    private let defaults = UserDefaults.standard
    
    static var instance = UserDefaultsManager()
    
    func saveUser(user: User?) {
        let userDic = try! user?.toDictionary(JSONEncoder())
        let userAsString = userDic?.toString()
        defaults.setValue(userAsString, forKey: UserDefaultsConstants.user)
        defaults.synchronize()
    }

    func getUser() -> User? {
        let userAsString = defaults.string(forKey: UserDefaultsConstants.user)
        return User.instance(dictionary: userAsString?.convertStringToDictionary())
    }

    func removeAllData() {
        defaults.removeObject(forKey: UserDefaultsConstants.user)
    }
}
