//
//  CommonConstants.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

struct UserDefaultsConstants {
    public static let user = "User"
}

struct APIUrlsConstants {
    public static let API_URL = "https://api.bitbucket.org/2.0/"
    public static let GET_USER_URL = "user"
    public static let GET_REPOSITORIES_URL = "repositories/"
    public static let GET_WORKSPACES_URL = "workspaces/"
}

struct ErrorMessages {
    public static let invalidLoginCredintials = "Invalid username or password"
    public static let invalidToken = "Invalid token"
}
