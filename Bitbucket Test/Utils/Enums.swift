//
//  Enums.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

public enum SplashNavigation {
    case DEFAULT
    case MAIN
    case LOGIN
}

public enum LoginNavigation {
    case DEFAULT
    case MAIN
}

public enum LoginViewStatus {
    case DEFAULT
    case USERNAME_ERROR
    case PASSWORD_ERROR
}
