//
//  Navigator.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

import UIKit

public class Navigator {
    
    var navigationController: UINavigationController!
    
    public init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    func navigateToLogin() {
        let vc = LoginViewController.buildVC()
        navigationController.viewControllers = [vc]
    }
    
    public func navigateToMainScreen() {
        let vc = MainTabBarViewController.buildVC()
        navigationController.viewControllers = [vc]
    }
    
    public func navigateToRepositoryDetails(repository: Repository) {
        let vc = RepositoryDetailsViewController.buildVC(repository: repository)
        navigationController.pushViewController(vc, animated: true)
    }
}
