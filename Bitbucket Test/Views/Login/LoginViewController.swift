//
//  ViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

class LoginViewController: BaseViewController {
    
    public class func buildVC() -> LoginViewController {
         let storyboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
         return storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    }
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var usernameTextField: UITextField! {
        didSet {
            usernameTextField.delegate = self
        }
    }
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.delegate = self
        }
    }
    
    let viewModel = UserViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleloginViewStatusChange()
        handleErrorMessage()
        handleNavigation()
        handleLoaderStatus()
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        viewModel.login(username: usernameTextField.text, password: passwordTextField.text)
    }
    
    private func handleloginViewStatusChange() {
        viewModel.loginViewStatus.bind { [weak self] status in
            switch status {
            case .USERNAME_ERROR:
                self?.usernameView.borderColor = UIColor.red
                break
                
            case .PASSWORD_ERROR:
                self?.passwordView.borderColor = UIColor.red
                break
                
            default:
                break
            }
        }
    }
    
    private func handleErrorMessage() {
        viewModel.errorMessage.bind { [weak self] message in
            self?.showToastMessage(message: message)
        }
    }
    
    private func handleNavigation() {
        viewModel.loginNavigation.bind { [weak self] loginNavigation in
            if loginNavigation == .MAIN {
                self?.navigator.navigateToMainScreen()
            }
        }
    }
    
    private func handleLoaderStatus() {
        viewModel.loaderStatus.bind { [weak self] showLoader in
            if showLoader {
                self?.showLoader()
            } else {
                self?.hideLoader()
            }
        }
    }
}

