//
//  LoginViewConotroller+Extensions.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            usernameView.borderColor = UIColor(hexString: "#0747A6")
        } else if textField == passwordTextField {
            passwordView.borderColor = UIColor(hexString: "#0747A6")
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == usernameTextField {
            usernameView.borderColor = UIColor(hexString: "#9BA7B7")
        } else if textField == passwordTextField {
            passwordView.borderColor = UIColor(hexString: "#9BA7B7")
        }
    }
}
