//
//  SettingsViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

class SettingsViewController: BaseViewController {
    
    public class func buildVC() -> SettingsViewController {
         let storyboard = UIStoryboard(name: "SettingsStoryboard", bundle: nil)
         return storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    }
    
    let viewModel = UserViewModel()
    
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userUsernameLabel: UILabel!
    @IBOutlet weak var accountIdLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        handleLoaderStatus()
        
        
        viewModel.getUserData(encodedAuth: UserDefaultsManager.instance.getUser()?.encodedAuth)
        
        viewModel.user.bind { [weak self] user in
            if let avatar = user.links?.avatar?.href, let url = URL(string: avatar) {
                self?.userPhotoImageView.af_setImage(withURL: url)
            }
            
            self?.userFullNameLabel.text = user.displayName
            self?.userUsernameLabel.text = user.username
            self?.accountIdLabel.text = "Account id: \(user.accountId ?? "")"
        }
    }

    @IBAction func logoutButtonPressed(_ sender: Any) {
        navigator.navigateToLogin()
        UserDefaultsManager.instance.removeAllData()
    }
    
    private func handleLoaderStatus() {
        viewModel.loaderStatus.bind { [weak self] showLoader in
            if showLoader {
                self?.showLoader()
            } else {
                self?.hideLoader()
            }
        }
    }
}
