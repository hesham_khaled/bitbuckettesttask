//
//  BaseViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit
import Toast_Swift
import NVActivityIndicatorView

class BaseViewController: UIViewController {
    
    public var navigator: Navigator!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenClick()
        if let navController = self.navigationController  {
            navigator = Navigator(navController: navController)
        }
    }
    
    func showToastMessage(message: String) {
        view.makeToast(message)
    }
    
    func hideKeyboardWhenClick() {
        self.view.addTapGesture { [weak self] recognizer in
            self?.dismissKeyboard()
        }
    }
    
    //EZSE: Dismisses keyboard
    @objc open func dismissKeyboard() {
        view.endEditing(true)
    }

    func showLoader() {
        let activityData = ActivityData(size: nil, message: nil, messageFont: nil, messageSpacing: nil, type: nil, color: UIColor.gray, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    func hideLoader() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
