//
//  SplashViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

class SplashViewController: BaseViewController {
    
    public class func buildVC() -> SplashViewController {
         let storyboard = UIStoryboard(name: "SplashStoryboard", bundle: nil)
         return storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
    }
    
    private let viewModel = UserViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.splashNavigation.bind { [weak self] splashNavigation in
            switch splashNavigation {
            case .LOGIN:
                self?.navigator.navigateToLogin()
                break
                
            case .MAIN:
                self?.navigator.navigateToMainScreen()
                break
                
            default:
                break
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.viewModel.decideSplashNavigation()
        }
    }

}
