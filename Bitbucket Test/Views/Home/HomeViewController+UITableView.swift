//
//  HomeViewController+UITableView.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoriesViewModel.repositoriesPaginatedResponse.value.values?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryCell.identifier, for: indexPath) as! RepositoryCell
        cell.repository = repositoriesViewModel.repositoriesPaginatedResponse.value.values?[indexPath.row]
        cell.addTapGesture { [weak self] _ in
            self?.navigator.navigateToRepositoryDetails(repository: cell.repository)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == ((repositoriesViewModel.repositoriesPaginatedResponse.value.values?.count ?? 0) - 1) else {return}
        repositoriesViewModel.getRepositories(workspaceUUID: selectedWorkspaceUUID, role: nil)
    }
}
