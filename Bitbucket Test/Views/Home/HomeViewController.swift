//
//  HomeViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit
import FittedSheets

class HomeViewController: BaseViewController {
    
    public class func buildVC() -> HomeViewController {
         let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
         return storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    }
    
    let repositoriesViewModel = RepositoryViewModel()
    let workspacesViewModel = WorkspaceViewModel()
    
    var selectedWorkspaceUUID: String? = nil

    @IBOutlet weak var filterByWorkspacesView: UIView! {
        didSet {
            filterByWorkspacesView.addTapGesture { [weak self] _ in
                guard let self = self else {return}
                let sheetController = SheetViewController(controller: WorkspacesViewController.buildVC(workspaces: self.workspacesViewModel.workspacesPaginatedResponse.value.values ?? [], delegate: self), sizes: [.fixed(350)])
                sheetController.blurBottomSafeArea = false
                sheetController.adjustForBottomSafeArea = true
                sheetController.topCornersRadius = 15
                self.navigator.navigationController.present(sheetController, animated: true, completion: nil)
            }
        }
    }
    @IBOutlet weak var selectedWorkspaceNameLabel: UILabel!
    @IBOutlet weak var repositoriesTableView: UITableView! {
        didSet {
            let nib = UINib(nibName: RepositoryCell.identifier, bundle: nil)
            repositoriesTableView.register(nib, forCellReuseIdentifier: RepositoryCell.identifier)
            repositoriesTableView.dataSource = self
            repositoriesTableView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleLoaderStatus()

        workspacesViewModel.getWorkspaces()
        
        repositoriesViewModel.repositoriesPaginatedResponse.bind { [weak self] repositoriesPaginatedResponse in
            self?.repositoriesTableView.reloadData()
        }
        repositoriesViewModel.getRepositories(workspaceUUID: selectedWorkspaceUUID, role: nil)
    }

    private func handleLoaderStatus() {
        repositoriesViewModel.loaderStatus.bind { [weak self] showLoader in
            if showLoader {
                self?.showLoader()
            } else {
                self?.hideLoader()
            }
        }
    }
}

extension HomeViewController: WorkspacesDelegate {
    func workspaceChoosen(index: Int) {
        let selectedWorkspace = workspacesViewModel.workspacesPaginatedResponse.value.values?[index]
        selectedWorkspaceNameLabel.text = selectedWorkspace?.name
        selectedWorkspaceUUID = selectedWorkspace?.uuid
        repositoriesViewModel.getRepositories(workspaceUUID: selectedWorkspaceUUID, role: nil)
    }
    
    func requestNewPage() {
        workspacesViewModel.getWorkspaces()
    }
}
