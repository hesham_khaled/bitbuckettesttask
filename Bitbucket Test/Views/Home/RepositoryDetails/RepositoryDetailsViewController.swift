//
//  RepositoryDetailsViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

class RepositoryDetailsViewController: BaseViewController {
    
    public class func buildVC(repository: Repository) -> RepositoryDetailsViewController {
         let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "RepositoryDetailsViewController") as! RepositoryDetailsViewController
        vc.repository = repository
        return vc
    }
    
    var repository: Repository!
    
    @IBOutlet weak var backImageView: UIImageView! {
        didSet {
            backImageView.addTapGesture { [weak self] _ in
                self?.navigator.navigationController.popViewController(animated: true)
            }
        }
    }
    @IBOutlet weak var repositoryImageView: UIImageView! {
        didSet {
            if let avatar = repository.links?.avatar?.href, let url = URL(string: avatar) {
                repositoryImageView.af_setImage(withURL: url)
            }
        }
    }
    @IBOutlet weak var repositoryNameLabel: UILabel! {
        didSet {
            repositoryNameLabel.text = repository.name
        }
    }
    @IBOutlet weak var repositoryCreatorLabel: UILabel! {
        didSet {
            repositoryCreatorLabel.text = "\(repository.owner?.displayName ?? "") / \(repository.project?.name ?? "")"
        }
    }
    @IBOutlet weak var repositoryDescLabel: UILabel! {
        didSet {
            if let desc = repository.description {
                repositoryDescLabel.text = desc
            } else {
                repositoryDescLabel.text = ""
            }
        }
    }
    @IBOutlet weak var repositoryIsPrivateLabel: UILabel! {
        didSet {
            if let isPrivate = repository.isPrivate, isPrivate {
                repositoryIsPrivateLabel.text = "This is a private repository"
            } else {
                repositoryIsPrivateLabel.text = "This is a public repository"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
