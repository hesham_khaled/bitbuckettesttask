//
//  WorkspacesViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

protocol WorkspacesDelegate {
    func workspaceChoosen(index: Int)
    func requestNewPage()
}

class WorkspacesViewController: BaseViewController {
    
    public class func buildVC(workspaces: [Workspace], delegate: WorkspacesDelegate?) -> WorkspacesViewController {
         let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WorkspacesViewController") as! WorkspacesViewController
        vc.workspaces = workspaces
        vc.delegate = delegate
         return vc
    }
    
    var workspaces: [Workspace] = []
    var delegate: WorkspacesDelegate?

    @IBOutlet weak var workspacesTableView: UITableView! {
        didSet {
            let nib = UINib(nibName: WorkspaceCell.identifier, bundle: nil)
            workspacesTableView.register(nib, forCellReuseIdentifier: WorkspaceCell.identifier)
            workspacesTableView.dataSource = self
            workspacesTableView.delegate = self
            workspacesTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
