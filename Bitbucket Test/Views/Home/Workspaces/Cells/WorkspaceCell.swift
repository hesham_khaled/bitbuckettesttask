//
//  WorkspaceCell.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

class WorkspaceCell: UITableViewCell {
    
    public static let identifier = "WorkspaceCell"

    @IBOutlet weak var workspaceNameLabel: UILabel!
    
    var workspace: Workspace! {
        didSet {
            workspaceNameLabel.text = workspace.name
        }
    }
    
}
