//
//  WorkspacesViewController+UITableView.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

extension WorkspacesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workspaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WorkspaceCell.identifier, for: indexPath) as! WorkspaceCell
        cell.workspace = workspaces[indexPath.row]
        cell.addTapGesture { [weak self] _ in
            self?.dismiss(animated: true, completion: {
                self?.delegate?.workspaceChoosen(index: indexPath.row)
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == (workspaces.count - 1) else {return}
        delegate?.requestNewPage()
    }
}
