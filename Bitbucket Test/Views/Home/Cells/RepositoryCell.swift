//
//  RepositoryCell.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit
import AlamofireImage

class RepositoryCell: UITableViewCell {
    
    public static let identifier = "RepositoryCell"

    @IBOutlet weak var repositoryImageView: UIImageView!
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var repositoryCreatorLabel: UILabel!
    
    var repository: Repository! {
        didSet {
            if let avatar = repository.links?.avatar?.href, let url = URL(string: avatar) {
                repositoryImageView.af_setImage(withURL: url)
            }
            
            repositoryNameLabel.text = repository.name
            repositoryCreatorLabel.text = "\(repository.owner?.displayName ?? "") / \(repository.project?.name ?? "")"
        }
    }
}
