//
//  MainTabBarViewController.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    
    public class func buildVC() -> MainTabBarViewController {
        let storyboard = UIStoryboard(name: "MainTabBarStoryboard", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
    }
    
    private let tabBarImagesNames = ["icHomeNormal", "icSettingsNormal"]
    private let selectedTabBarImagesNames = ["icHomePressed", "icSettingsPressed"]
    private let tabBarTitles = ["Home", "Settings"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        startViewControllers()
        customizeTabBar()
        self.selectedIndex = 0
    }

    private func customizeTabBar() {
        self.tabBar.barTintColor = UIColor.white
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        tabBar.backgroundColor = UIColor.white
        tabBar.tintColor = UIColor(hexString: "#199E91")
            
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 4
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
        for case let (index, item) in self.tabBar.items!.enumerated() {
            item.image = UIImage(named: tabBarImagesNames[index])?.withRenderingMode(.alwaysOriginal)
            item.selectedImage = UIImage(named: selectedTabBarImagesNames[index])?.withRenderingMode(.alwaysOriginal)
            item.title = tabBarTitles[index]
        }
    }
    
    private func startViewControllers() {
        self.viewControllers = [HomeViewController.buildVC(), SettingsViewController.buildVC()]
    }
}
