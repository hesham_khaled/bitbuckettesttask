//
//  RepositoryRepo.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Alamofire

protocol RepositoryRepo {
    func getRepositories(uuid: String, currentPage: Int, workspaceUUID: String?, role: String?, completionHandler: @escaping (PaginatedResponse<Repository>?) -> (), failureHandler: @escaping (Int?) -> ())
}

public class RepositoryRepoImpl: RepositoryRepo {
    func getRepositories(uuid: String, currentPage: Int, workspaceUUID: String?, role: String?, completionHandler: @escaping (PaginatedResponse<Repository>?) -> (), failureHandler: @escaping (Int?) -> ()) {
        
        var params = [String : Any]()
        params["page"] = currentPage
        if let role = role {
            params["role"] = role
        }
        
        var headers = HTTPHeaders()
                        
        var url = APIUrlsConstants.GET_REPOSITORIES_URL
        if let workspaceUUID = workspaceUUID {
            url += "\(workspaceUUID.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
            if let currentUser = UserDefaultsManager.instance.getUser() {
                headers = ["Authorization" : "Basic \(currentUser.encodedAuth ?? "")"]
            }
        } else {
            url += "\(uuid.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
        }
        
        NetworkingManager.sendRequest(method: .get, url: url, headers: headers, params: params) { data in
            do {
                let dataResponse: PaginatedResponse<Repository> = try JSONDecoder().decode(PaginatedResponse<Repository>.self, from: data!)
                completionHandler(dataResponse)
            } catch (let error) {
                print("error in parsing in RepositoryRepoImpl :: \(error.localizedDescription)")
            }
        } errorHandler: { statusCode in
            failureHandler(statusCode)
        }
    }
}
