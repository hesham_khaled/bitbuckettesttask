//
//  UserRepo.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Alamofire

protocol UserRepo {
    func login(authorization: String, completionHandler: @escaping (User?) -> (), failureHandler: @escaping (Int?) -> ())
    func saveUserToUserDefaults(user: User)
    func getUserFromUserDefaults() -> User?
}

public class UserRepoImpl: UserRepo {
    
    func login(authorization: String, completionHandler: @escaping (User?) -> (), failureHandler: @escaping (Int?) -> ()) {
        NetworkingManager.sendRequest(method: .get, url: APIUrlsConstants.GET_USER_URL, headers: ["Authorization" : "Basic \(authorization)"], params: nil) { data in
            do {
                let dataResponse: User = try JSONDecoder().decode(User.self, from: data!)
                
                completionHandler(dataResponse)
            } catch (let error) {
                print("error in parsing in UserRepoImpl :: \(error.localizedDescription)")
            }
        } errorHandler: { statusCode in
            failureHandler(statusCode)
        }
    }
    
    func saveUserToUserDefaults(user: User) {
        UserDefaultsManager.instance.saveUser(user: user)
    }
    
    func getUserFromUserDefaults() -> User? {
        return UserDefaultsManager.instance.getUser()
    }
}

