//
//  WorkspaceRepo.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Alamofire

protocol WorkspaceRepo {
    func getWorkspaces(currentPage: Int, completionHandler: @escaping (PaginatedResponse<Workspace>?) -> (), failureHandler: @escaping (Int?) -> ())
}

public class WorkspaceRepoImpl: WorkspaceRepo {
    func getWorkspaces(currentPage: Int, completionHandler: @escaping (PaginatedResponse<Workspace>?) -> (), failureHandler: @escaping (Int?) -> ()) {
        
        let currentUser = UserDefaultsManager.instance.getUser()
        
        NetworkingManager.sendRequest(method: .get, url: APIUrlsConstants.GET_WORKSPACES_URL, headers: ["Authorization" : "Basic \(currentUser?.encodedAuth ?? "")"], params: ["page":currentPage]) { data in
            do {
                let dataResponse: PaginatedResponse<Workspace> = try JSONDecoder().decode(PaginatedResponse<Workspace>.self, from: data!)
                completionHandler(dataResponse)
            } catch (let error) {
                print("error in parsing in RepositoryRepoImpl :: \(error.localizedDescription)")
            }
        } errorHandler: { statusCode in
            failureHandler(statusCode)
        }
    }
}
