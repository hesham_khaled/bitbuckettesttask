//
//  NetworkingManager.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

import Alamofire

class NetworkingManager {
    
    static func sendRequest(method: HTTPMethod, url: String, headers: HTTPHeaders? = nil,
                            params: [String: Any]? = nil, encoding: ParameterEncoding = URLEncoding.default,
                                successHandler: @escaping (_ response: Data?)->(),
                                errorHandler: @escaping (_ statusCode: Int?)->()) {

        var recievedHeaders = headers
        if recievedHeaders == nil {
            recievedHeaders = HTTPHeaders()
        }
        recievedHeaders?.add(name: "Content-Type", value: "application/json")
        
        AF.request(APIUrlsConstants.API_URL + url, method: method,parameters: params ?? [:], encoding: encoding, headers: recievedHeaders)
            .responseJSON(completionHandler: { response in
                print(APIUrlsConstants.API_URL + url)
                print(response)
                if response.response?.statusCode == 200 {
                    successHandler(response.data)
                    return
                } else {
                    errorHandler(response.response?.statusCode)
                }
            })
    }
}
