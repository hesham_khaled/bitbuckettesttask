//
//  PaginatedReponse.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

struct PaginatedResponse<T: Codable> : Codable {
    var pageLen: Int?
    var currentPage: Int?
    var size: Int?
    var values: [T]?
    var next: String?
    
    enum CodingKeys: String, CodingKey {
        case pageLen = "pagelen"
        case currentPage = "page"
        case size
        case values
        case next
    }
    
    init() {
        
    }
    
    init(paginatedResponse: PaginatedResponse?, isFiltered: Bool) {
        pageLen = paginatedResponse?.pageLen
        currentPage = paginatedResponse?.currentPage
        size = paginatedResponse?.size
        next = paginatedResponse?.next
        if let _ = values {
            if isFiltered {
                values?.append(contentsOf: paginatedResponse?.values ?? [])
            } else {
                values?.removeAll()
                values = paginatedResponse?.values ?? []
            }
            
        } else {
            values = paginatedResponse?.values ?? []
        }
    }
}
