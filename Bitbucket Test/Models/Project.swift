//
//  Project.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

struct Project: Codable {
    var links: Link?
    var type: String?
    var uuid: String?
    var key: String?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case type, uuid, links, key, name
    }
}
