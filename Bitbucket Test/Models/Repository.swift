//
//  Repository.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

public struct Repository: Codable {
    var scm: String?
    var website: String?
    var hasWiki: Bool?
    var uuid: String?
    var links: Link?
    var forkPolicy: String?
    var fullName: String?
    var name: String?
    var project: Project?
    var language: String?
    var createdOn: String?
    var mainBranch: Branch?
    var workspace: Workspace?
    var hasIssues: Bool?
    var owner: User?
    var updatedOn: String?
    var size: Int?
    var type: String?
    var slug: String?
    var isPrivate: Bool?
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case scm, website, hasWiki = "has_wiki", uuid, links,  forkPolicy = "fork_policy", fullName = "full_name", name, project, owner, language, createdOn = "created_on", mainBranch = "mainbranch", workspace, hasIssues = "has_issues", updatedOn = "updated_on", size, type, slug, isPrivate = "is_private", description
    }
}
