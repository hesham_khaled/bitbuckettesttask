//
//  User.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

struct User: Codable {
    var username: String?
    var displayName: String?
    var accountId: String?
    var nickname: String?
    var isStaff: Bool?
    var accountStatus: String?
    var type: String?
    var uuid: String?
    var links: Link?
    var encodedAuth: String? // this is for saving the encoded basic auth header to be used in calling apis
    
    enum CodingKeys: String, CodingKey {
        case username, displayName = "display_name", accountId = "account_id", nickname, isStaff = "is_staff", accountStatus = "account_status", type, uuid, links, encodedAuth
    }
    
    public static func instance(dictionary: [String: Any]?) -> User? {
        if let dictionary = dictionary {
            return User(dictionary: dictionary)
        } else {
            return nil
        }
    }
    
    init(dictionary: [String: Any]) {
        username = dictionary["username"] as? String
        displayName = dictionary["display_name"] as? String
        accountId = dictionary["account_id"] as? String
        nickname = dictionary["nickname"] as? String
        isStaff = dictionary["is_staff"] as? Bool
        accountStatus = dictionary["account_status"] as? String
        type = dictionary["type"] as? String
        uuid = dictionary["uuid"] as? String
        links = dictionary["links"] as? Link
        encodedAuth = dictionary["encodedAuth"] as? String
    }
    
    init() {
        
    }
}
