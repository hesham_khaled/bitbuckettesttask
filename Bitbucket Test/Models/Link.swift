//
//  Link.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

struct Link: Codable {
    var repositories: LinkType?
    var avatar: LinkType?
    
    
    enum CodingKeys: String, CodingKey {
        case repositories, avatar
    }
}

struct LinkType: Codable {
    var href: String?
    
    enum CodingKeys: String, CodingKey {
        case href
    }
}
