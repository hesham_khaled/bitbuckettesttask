//
//  Branch.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

struct Branch: Codable {
    var name: String?
    var type: String?
    
    enum CodingKeys: String, CodingKey {
        case name, type
    }
}
