//
//  RepositoryViewModel.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

class RepositoryViewModel: BaseViewModel {
    
    let repositoryRepo: RepositoryRepo = RepositoryRepoImpl()
    let userRepo: UserRepo = UserRepoImpl()
    var repositoriesPaginatedResponse = Binder(PaginatedResponse<Repository>())
    
    var selectedWorkspaceUUID: String? = nil
    
    func getRepositories(workspaceUUID: String?, role: String?) {
        if let workspaceUUID = workspaceUUID, selectedWorkspaceUUID != workspaceUUID {
            repositoriesPaginatedResponse.value.currentPage = nil
            selectedWorkspaceUUID = workspaceUUID
        }
        
        if let uuid = userRepo.getUserFromUserDefaults()?.uuid, (repositoriesPaginatedResponse.value.currentPage == nil || (repositoriesPaginatedResponse.value.next != nil && !(repositoriesPaginatedResponse.value.next?.isEmpty ?? true)))  {
            loaderStatus.value = true
            var page = 1
            if repositoriesPaginatedResponse.value.currentPage != nil && repositoriesPaginatedResponse.value.currentPage ?? 0 > 0 {
                page += 1
            }
            repositoryRepo.getRepositories(uuid: uuid, currentPage: page, workspaceUUID: workspaceUUID, role: role) { [weak self] paginatedResponse in
                self?.loaderStatus.value = false
                self?.repositoriesPaginatedResponse.value = PaginatedResponse(paginatedResponse: paginatedResponse, isFiltered: workspaceUUID != nil)
            } failureHandler: { [weak self] _ in
                self?.loaderStatus.value = false
            }
        }
    }
}
