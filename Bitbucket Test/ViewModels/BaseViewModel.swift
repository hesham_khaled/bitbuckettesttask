//
//  File.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

class BaseViewModel {
    
    var errorMessage = Binder(String())
    var loaderStatus = Binder(Bool())
    
}
