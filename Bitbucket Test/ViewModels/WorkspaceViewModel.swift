//
//  WorkspaceViewModel.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

class WorkspaceViewModel: BaseViewModel {
    
    let workspaceRepo: WorkspaceRepo = WorkspaceRepoImpl()
    var workspacesPaginatedResponse = Binder(PaginatedResponse<Workspace>())
    
    func getWorkspaces() {
        if (workspacesPaginatedResponse.value.currentPage == nil || (workspacesPaginatedResponse.value.next != nil && !(workspacesPaginatedResponse.value.next?.isEmpty ?? true)))  {
            loaderStatus.value = true
            var page = 1
            if workspacesPaginatedResponse.value.currentPage != nil && workspacesPaginatedResponse.value.currentPage ?? 0 > 0 {
                page += 1
            }
            workspaceRepo.getWorkspaces(currentPage: page) { [weak self] paginatedResponse in
                self?.loaderStatus.value = false
                
                self?.workspacesPaginatedResponse.value = PaginatedResponse(paginatedResponse: paginatedResponse, isFiltered: false)
                
            } failureHandler: { [weak self] _ in
                self?.loaderStatus.value = false
            }
        }
    }
}
