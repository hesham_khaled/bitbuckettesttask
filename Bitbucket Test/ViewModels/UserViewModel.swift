//
//  UserViewModel.swift
//  Bitbucket Test
//
//  Created by Hesham Donia on 18/09/2021.
//

import Foundation

class UserViewModel: BaseViewModel {
    
    var splashNavigation = Binder(SplashNavigation.DEFAULT)
    var loginNavigation = Binder(LoginNavigation.DEFAULT)
    var loginViewStatus = Binder(LoginViewStatus.DEFAULT)
    var user = Binder(User())
    
    let userRepo: UserRepo = UserRepoImpl()
    
    func decideSplashNavigation() {
        if let _ = UserDefaultsManager.instance.getUser() {
            splashNavigation.value = .MAIN
        } else {
            splashNavigation.value = .LOGIN
        }
    }
    
    func login(username: String?, password: String?) {
        if let username = username, !username.isEmpty, let password = password, !password.isEmpty {
            loaderStatus.value = true
            let encodedAuth = "\(username):\(password)".toBase64()
            userRepo.login(authorization: encodedAuth) { [weak self] user in
                self?.loaderStatus.value = false
                if let _ = user {
                    var userToBeChnaged = user!
                    userToBeChnaged.encodedAuth = encodedAuth
                    self?.userRepo.saveUserToUserDefaults(user: userToBeChnaged)
                    self?.loginNavigation.value = LoginNavigation.MAIN
                } else {
                    self?.errorMessage.value = ErrorMessages.invalidLoginCredintials
                }
            } failureHandler: { [weak self] _ in
                self?.loaderStatus.value = false
                self?.errorMessage.value = ErrorMessages.invalidLoginCredintials
            }

        } else {
            if username?.isEmpty ?? true {
                loginViewStatus.value = LoginViewStatus.USERNAME_ERROR
            }
            
            if password?.isEmpty ?? true {
                loginViewStatus.value = LoginViewStatus.PASSWORD_ERROR
            }
        }
    }
    
    func getUserData(encodedAuth: String?) {
        loaderStatus.value = true
        userRepo.login(authorization: encodedAuth ?? "") { [weak self] user in
            self?.loaderStatus.value = false
            if let _ = user {
                var userToBeChnaged = user!
                userToBeChnaged.encodedAuth = encodedAuth
                self?.user.value = user ?? User()
            } else {
                self?.errorMessage.value = ErrorMessages.invalidLoginCredintials
            }
        } failureHandler: { [weak self] _ in
            self?.loaderStatus.value = false
            self?.errorMessage.value = ErrorMessages.invalidLoginCredintials
        }

    }
    
    
    
}
